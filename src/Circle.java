public class Circle extends Shape {
    private double radius;
    public Circle(double radius) {
        super("Circle");
        this.radius = radius;
    }
    @Override
    public double calArea() {
        return Math.PI * this.radius * this.radius;
    }
    @Override
    public double calPerimeter() {
        return 2 * Math.PI * this.radius;
    }
    public String toString() {
        return this.getName() + " radius:" + this.radius;
    }
}
