public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rec = new Rectangle(5,3);
        System.out.println(rec.toString());
        System.out.printf("%s area : %.1f \n", rec.getName(),rec.calArea());
        System.out.printf("%s perimeter : %.1f \n", rec.getName(),rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2,2);
        System.out.println(rec2.toString());
        System.out.printf("%s area : %.1f \n", rec2.getName(),rec2.calArea());
        System.out.printf("%s area : %.1f \n", rec2.getName(),rec2.calPerimeter());
        System.out.println();
        Circle cir1 = new Circle(2);
        System.out.println(cir1.toString());
        System.out.printf("%s area : %.3f \n", cir1.getName(),cir1.calArea());
        System.out.printf("%s perimeter : %.3f \n", cir1.getName(),cir1.calPerimeter());

        Circle cir2 = new Circle(3);
        System.out.println(cir2.toString());
        System.out.printf("%s area : %.3f \n", cir2.getName(),cir2.calArea());
        System.out.printf("%s perimeter : %.3f \n", cir2.getName(),cir2.calPerimeter());
        System.out.println();
        Triangle tri1 = new Triangle(2,2,2);
        System.out.println(tri1.toString());
        System.out.printf("%s area : %.3f \n", tri1.getName(),tri1.calArea());
        System.out.printf("%s area : %.0f \n", tri1.getName(),tri1.calPerimeter());
    }
}
